provider "aws" {
    region = "us-east-1"
    access_key = ""
    secret_key = ""
} 

resource "aws_s3_bucket" "my_cool_bucket"  {
    bucket = "globo-deped-bucket"
    acl = "public-read"
    tags {
        Name = "Terraform demo"
        Environment = "Prod"
        TF = "yes"
    }
}
